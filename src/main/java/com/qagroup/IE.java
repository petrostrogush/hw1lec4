package com.qagroup;

public class IE extends Browser {

	public IE() {
		super("IE");
	}	
	
	@Override
	public void Open() {
		System.out.println(toString() + " is opened. And it is glitching");
	}
}
