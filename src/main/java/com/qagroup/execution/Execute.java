package com.qagroup.execution;

import com.qagroup.Chrome;
import com.qagroup.Firefox;
import com.qagroup.IE;

public class Execute {

	public static void main(String[] args) {
		Chrome Chrome = new Chrome();
		Chrome.Open();
		
		Firefox Firefox = new Firefox();
		Firefox.Open();
		
		IE IE = new IE();
		IE.Open();
	}

}
