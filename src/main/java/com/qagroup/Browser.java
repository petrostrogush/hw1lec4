package com.qagroup;

public class Browser {

	private String name;
	
	public Browser(String name) {
		this.name = name;
	}
	
	public void Open() {
		System.out.println(this.name + " is opened");
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
